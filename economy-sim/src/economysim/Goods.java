package economysim;

import java.util.Arrays;

import repast.simphony.random.RandomHelper;

public class Goods {
	public static final int GOODS_COUNT = 10;
	public static final double MAX_CONSUMPTION = 0.5;

	public static Goods getRandomGoods() {
		Goods goods = new Goods();
		goods.money = 100;
		for (int i = 0; i < GOODS_COUNT; i++) {
			goods.goods[i] = RandomHelper.nextDoubleFromTo(0, 1);
		}
		return goods;
	}

	/**
	 * @param from
	 *            first trader
	 * @param to
	 *            second trader
	 * @param amounts
	 *            how much first gives to second (can be negative), trader needs
	 *            to have enough goods
	 * @param price
	 *            how much second pays to first (can be negative), trader can
	 *            have less money, transaction would be scaled down
	 */
	public static void transaction(Goods from, Goods to, double[] amounts,
			double price) {
		double multiplier = 1;
		if (to.money < price) {
			multiplier = to.money / price;
		}
		if (from.money < -price) {// money goes the other way
			multiplier = from.money / (-price);
		}
		System.out.print("Traded: ");
		multiplier = Math.nextAfter(multiplier, 0);// safety first
		for (int i = 0; i < GOODS_COUNT; i++) {
			double amount = multiplier * amounts[i];
			from.goods[i] -= amount;
			to.goods[i] += amount;
			System.out.print(amount+" of "+i+"; ");
		}
		System.out.println();
		from.money += multiplier * price;
		to.money -= multiplier * price;
	}

	private double money;
	private double goods[] = new double[GOODS_COUNT];

	public double getMoney() {
		return money;
	}

	public double[] getGoods() {
		return goods;
	}

	/**
	 * Consumes goods. Can consume at most {@link #MAX_CONSUMPTION} of each
	 * goods. Moreover, you cannot consume more goods[i] than you consumed
	 * goods[i-1] - higher-indexed goods are more luxurious.
	 * 
	 * @return happiness after the consumption
	 */
	public double consume() {
		double result = 0;
		double[] consumption = getConsumable();
		for (int i = 0; i < GOODS_COUNT; i++) {
			goods[i] -= consumption[i];
			result += i * consumption[i];
		}
		return result;
	}

	public double[] getConsumable() {
		double[] result = new double[GOODS_COUNT];
		double consumption = MAX_CONSUMPTION;
		for (int i = 0; i < GOODS_COUNT; i++) {
			if (goods[i] < consumption) {
				consumption = goods[i];
			}
			result[i] = consumption;
		}
		return result;
	}

	public void validate() {
		for (double d : goods) {
			if (!(d >= 0)) {
				throw new RuntimeException("d=" + d);
			}
		}
		if (!(money >= 0)) {
			throw new RuntimeException("m=" + money);
		}
	}

	public double getTotalAmount() {
		double sum = 0;
		for (double d : goods) {
			sum += d;
		}
		return sum;
	}

	public double getMeanAmount() {
		return getTotalAmount() / GOODS_COUNT;
	}

	public String toString() {
		return getClass().getName() + "[m=" + money + ",g="
				+ Arrays.toString(goods) + "]";
	}
}
