package economysim;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

public final class SimulationParameters {
	public static final String GRID_SIZE = "grid_size";
	public static final String HUMAN_COUNT = "human_count";
	public static final String GOODS_COUNT = "goods_count";
	public static final String CONSUMPTION_FACTOR = "consumption_factor";
	public static final String MAX_CONSUMPTION = "max_consumption";
	public static final String MAX_CAPACITY = "max_capacity";
	public static final String TALENT_MEAN = "talent_mean";
	public static final String TALENT_STDEV = "talent_stdev";
	public static final String TRADING_ENABLED = "trading_enabled";
	public static final String SHARING_ENABLED = "sharing_enabled";
	public static final String LUXURY_LEVELS_ENABLED = "luxury_levels_enabled";
	public static final String EXPERIENCE_FACTOR = "experience_factor";

	private static Parameters params = null;

	public static Object getParam(String name) {
		if (params == null) {
			params = RunEnvironment.getInstance().getParameters();
		}
		return params.getValue(name);
	}

	public static int getIntParam(String name) {
		return (Integer) getParam(name);
	}
	
	public static double getDoubleParam(String name) {
		return (Double) getParam(name);
	}
	
	public static boolean getBooleanParam(String name) {
		return (Boolean) getParam(name);
	}
}
