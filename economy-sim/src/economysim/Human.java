package economysim;

import static economysim.SimulationParameters.CONSUMPTION_FACTOR;
import static economysim.SimulationParameters.GOODS_COUNT;
import static economysim.SimulationParameters.LUXURY_LEVELS_ENABLED;
import static economysim.SimulationParameters.MAX_CAPACITY;
import static economysim.SimulationParameters.MAX_CONSUMPTION;
import static economysim.SimulationParameters.SHARING_ENABLED;
import static economysim.SimulationParameters.TALENT_MEAN;
import static economysim.SimulationParameters.TALENT_STDEV;
import static economysim.SimulationParameters.TRADING_ENABLED;
import static economysim.SimulationParameters.getBooleanParam;
import static economysim.SimulationParameters.getDoubleParam;
import static economysim.SimulationParameters.getIntParam;

import java.util.Arrays;
import java.util.List;

import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.grid.GridCell;
import repast.simphony.query.space.grid.GridCellNgh;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import cern.jet.random.Normal;

public class Human {
	private final ContinuousSpace<Object> space;
	private final Grid<Object> grid;

	private final double[] goods = new double[getIntParam(GOODS_COUNT)];
	private final double[] talent = new double[getIntParam(GOODS_COUNT)];

	private long trades = 0;
	private long steps = 0;
	private double happiness = 0;

	public Human(ContinuousSpace<Object> space, Grid<Object> grid) {
		this.space = space;
		this.grid = grid;

		Normal normalDist = RandomHelper.createNormal(
				getDoubleParam(TALENT_MEAN), getDoubleParam(TALENT_STDEV));
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			goods[i] = 0.5;
			talent[i] = Math.max(0, normalDist.nextDouble());
		}
	}

	@ScheduledMethod(start = 1, interval = 1, priority = Integer.MAX_VALUE)
	public void initStep() {
		steps++;
	}

	@ScheduledMethod(start = 1, interval = 1, priority = 120)
	public void consume() {
		happiness = 0;
		int goodsCount = getIntParam(GOODS_COUNT);
		boolean luxuryLevels = getBooleanParam(LUXURY_LEVELS_ENABLED);
		Double[] consumptions = new Double[goodsCount];
		for (int i = 0; i < goodsCount; i++) {
			consumptions[i] = Math.min(goods[i]
					* getDoubleParam(CONSUMPTION_FACTOR),
					getDoubleParam(MAX_CONSUMPTION));
			if (luxuryLevels && i > 0) {
				consumptions[i] = Math
						.min(consumptions[i - 1], consumptions[i]);
			}
			goods[i] -= consumptions[i];
		}
		Arrays.sort(consumptions);
		for (int i = goodsCount - 1; i >= 0; i--) {
			happiness += (goodsCount - i) * consumptions[i];
		}
	}

	@ScheduledMethod(start = 1, interval = 1, priority = 110)
	public void produce() {
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			goods[i] += Math.min(talent[i], getDoubleParam(MAX_CAPACITY)
					- goods[i]);
		}
	}

	@ScheduledMethod(start = 1, interval = 1, priority = 100)
	public void move() {
		steps++;
		GridPoint pt = grid.getLocation(this);

		GridCellNgh<Human> nghCreator = new GridCellNgh<Human>(grid, pt,
				Human.class, 2, 2);
		List<GridCell<Human>> gridCells = nghCreator.getNeighborhood(true);

		GridPoint destination = gridCells.get(
				RandomHelper.nextIntFromTo(0, gridCells.size() - 1)).getPoint();
		moveTowards(destination);
	}

	public void moveTowards(GridPoint pt) {
		if (!pt.equals(grid.getLocation(this))) {
			NdPoint myPoint = space.getLocation(this);
			NdPoint otherPoint = new NdPoint(pt.getX(), pt.getY());
			double angle = SpatialMath.calcAngleFor2DMovement(space, myPoint,
					otherPoint);
			space.moveByVector(this, 1, angle, 0);
			myPoint = space.getLocation(this);
			grid.moveTo(this, (int) myPoint.getX(), (int) myPoint.getY());
		}
	}

	@ScheduledMethod(start = 1, interval = 1, priority = 90)
	public void trade() {
		if (getBooleanParam(TRADING_ENABLED)) {
			int[] myLocation = grid.getLocation(this).toIntArray(null);
			for (Object obj : grid.getObjectsAt(myLocation)) {
				if (obj instanceof Human && obj != this) {
					tradeWith((Human) obj);
				}
			}
		}
	}

	@ScheduledMethod(start = 1, interval = 1, priority = 80)
	public void share() {
		if (getBooleanParam(SHARING_ENABLED)) {
			int[] myLocation = grid.getLocation(this).toIntArray(null);
			for (Object obj : grid.getObjectsAt(myLocation)) {
				if (obj instanceof Human && obj != this) {
					shareWith((Human) obj);
				}
			}
		}
	}

	private void tradeWith(Human other) {
		trades++;
		double threshold = getDoubleParam(MAX_CONSUMPTION)
				/ getDoubleParam(CONSUMPTION_FACTOR);
		double[] transfer = new double[getIntParam(GOODS_COUNT)];
		double totaltaken = 0, totalgiven = 0;
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			double offer1 = goods[i] - threshold;
			double offer2 = other.goods[i] - threshold;
			if (offer1 > 0 && offer2 < 0) {
				transfer[i] = -Math.min(offer1, -offer2);
			} else if (offer1 < 0 && offer2 > 0) {
				transfer[i] = Math.min(-offer1, offer2);
			} else {
				transfer[i] = 0;
			}
			if (transfer[i] > 0) {
				totaltaken += transfer[i];
			} else {
				totalgiven += -transfer[i];
			}
		}
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			if (totaltaken > totalgiven && transfer[i] > 0) {
				transfer[i] *= (totalgiven / totaltaken);
			} else if (totalgiven > totaltaken && transfer[i] < 0) {
				transfer[i] *= (totaltaken / totalgiven);
			}
		}
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			goods[i] += transfer[i];
			other.goods[i] -= transfer[i];
		}
	}

	private void shareWith(Human other) {
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			goods[i] = other.goods[i] = (goods[i] + other.goods[i]) / 2;
		}
	}

	public double getHappiness() {
		return Math.max(0.01, happiness);
	}

	public double getGoods() {
		double result = 0;
		for (double g : goods) {
			result += g;
		}
		return result;
	}

	public double getMeanTrades() {
		return (double) trades / steps;
	}

	public static void main(String[] args) {
		Human human1 = new Human(null, null);
		Human human2 = new Human(null, null);
		Normal normalDist = RandomHelper.createNormal(1.6, 0.3);
		for (int i = 0; i < getIntParam(GOODS_COUNT); i++) {
			human1.goods[i] = normalDist.nextDouble();
			human2.goods[i] = normalDist.nextDouble();
		}
		System.out.println(Arrays.toString(human1.goods));
		System.out.println(Arrays.toString(human2.goods));
		human1.tradeWith(human2);
		System.out.println(Arrays.toString(human1.goods));
		System.out.println(Arrays.toString(human2.goods));
		human1.tradeWith(human2);
		System.out.println(Arrays.toString(human1.goods));
		System.out.println(Arrays.toString(human2.goods));
	}
}
