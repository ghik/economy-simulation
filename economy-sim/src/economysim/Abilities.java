package economysim;

import java.util.Random;

public class Abilities {
	public static double PRODUCTION_FACTOR = 1.0;

	private Random r = new Random();

	private double abilities[] = new double[Goods.GOODS_COUNT];
	private double produced[] = new double[Goods.GOODS_COUNT];

	public Abilities() {
		for (int i = 0; i < Goods.GOODS_COUNT; i++) {
			abilities[i] = r.nextDouble();
			produced[i] = 0;
		}
	}

	public void produceGoods(Goods goods, double happiness) {
		for (int i = 0; i < Goods.GOODS_COUNT; i++) {
			double producedNow = abilities[i] * PRODUCTION_FACTOR;
			goods.getGoods()[i] += producedNow;
			produced[i] += producedNow;
		}
	}
}
